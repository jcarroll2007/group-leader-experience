(function () {
    'use strict';

    function footer() {
        return {
            restrict: 'E',
            templateUrl: 'components/footer/footer.html',
            controller: angular.noop,
            scope: {}
        };
    }

    angular.module('components.footer', [])
        .directive('footer', footer);
}());