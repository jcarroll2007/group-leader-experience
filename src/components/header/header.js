(function () {
    'use strict';

    function header() {
        return {
            restrict: 'E',
            templateUrl: 'components/header/header.html',
            controller: angular.noop,
            scope: {}
        };
    }

    angular.module('components.header', [])
        .directive('header', header);
}());