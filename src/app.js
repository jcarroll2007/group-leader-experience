(function () {
    'use strict';

    angular.module('teamworks', [
        'ui.router',
        'templates',
        'app.routes',
        'app.components'
    ]);

}());