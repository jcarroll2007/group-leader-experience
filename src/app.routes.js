(function () {
    'use strict';

    function config($stateProvider, $urlRouterProvider) {

        ///////////////////////////
        // Redirects and Otherwise
        ///////////////////////////
        $urlRouterProvider
            .otherwise('/');

        ///////////////////////////
        // State Configurations
        ///////////////////////////

        $stateProvider


            ///////////////////////////
            // Home
            ///////////////////////////
            .state('app', {
                url: '/',
                templateUrl: 'app/app.html'
            })

            ///////////////////////////
            // About
            ///////////////////////////
            .state('app.about', {
                url: 'about',
                templateUrl: 'app/about/about.html'
            })

            ///////////////////////////
            // Signup
            ///////////////////////////
            .state('signup', {
                url: '/signup',
                template: '<signup></signup>'
            })

            ///////////////////////////
            // Home
            ///////////////////////////
            .state('home', {
                url: '/home',
                templateUrl: 'app/home/home.html',
                controller: 'HomeController',
                controllerAs: 'home'
            })

            .state('application', {
                url: '/application',
                template: '<fundraising-application></fundraising-application>'
            });
    }

    angular.module('app.routes', [
        'app.signup',
        'app.home',
        'app.application',
        'components.header',
        'components.footer',
        'ui.bootstrap'
    ])
        .config(config);
}());