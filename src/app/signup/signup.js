(function () {
    'use strict';

    function SignupController(AddGroupModal, Groups) {
        this.groups = Groups.getList();
        this.titles = [
            'Coach',
            'Fundraising Chair',
            'Booster Club Member',
            'Assistant',
            'Other'
        ];
        this.slides = [{
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2015/10/approval.jpg',
            title: 'manage all of your fundraisers.'
        }, {
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2015/10/funraising_coach.png',
            title: 'walk you through the process.'
        }, {
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2014/01/build-your-own1.jpg',
            title: 'sell your products online.'
        }, {
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2015/10/reports.png',
            title: 'create reports.'
        }];

        this.addGroup = AddGroupModal.show;
    }

    function signup() {
        return {
            restrict: 'E',
            templateUrl: 'app/signup/signup.html',
            controller: SignupController,
            controllerAs: 'ctrl',
            scope: {}
        };
    }

    angular.module('app.signup', [
        'app.signups.groups',
        'components.header',
        'components.footer',
        'ui.bootstrap'
    ])
        .directive('signup', signup);
}());