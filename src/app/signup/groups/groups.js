(function () {
    'use strict';

    function AddGroupController() {
        this.test = 'test';
    }

    function addGroup() {
        return {
            restrict: 'E',
            templateUrl: 'app/signup/groups/add.html',
            controller: AddGroupController,
            controllerAs: 'ctrl',
            scope: {}
        };
    }

    function AddGroupModalController() {
        this.test = 'test';
    }

    function AddGroupModal($modal) {
        this.show = function () {
            $modal.open({
                template: '<add-group></add-group>',
                contoller: AddGroupModalController
            });
        };
    }

    function Groups() {
        this.list = [
            'Band',
            'Baseball',
            'Track',
            'Football',
            'Chearleading'
        ];
        this.getList = function () {
            return this.list;
        };
    }

    angular.module('app.signups.groups', [])
        .directive('addGroup', addGroup)
        .service('AddGroupModal', AddGroupModal)
        .service('Groups', Groups);
}());