(function () {
    'use strict';

    function HomeController() {
        this.campaignManagersActive = true;
        this.welcomeActive = true;
        this.activeFundraiserState = 4;

        this.slides = [{
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2015/10/approval.jpg',
            title: 'manage all of your fundraisers.'
        }, {
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2015/10/funraising_coach.png',
            title: 'walk you through the process.'
        }, {
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2014/01/build-your-own1.jpg',
            title: 'sell your products online.'
        }, {
            image: 'http://www.myteamworks.org/home/wp-content/uploads/2015/10/reports.png',
            title: 'create reports.'
        }];
    }

    angular.module('app.home', [])
        .controller('HomeController', HomeController);
}());