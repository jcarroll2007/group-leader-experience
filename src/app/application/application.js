(function () {
    'use strict';

    function FundraisingApplication() {
        this.sections = [{
            title: 'First, let\'s get some basic information',
            name: 'initialInfo',
            step: 1
        }, {
            title: 'Dates',
            name: 'dates',
            step: 2
        }, {
            title: 'How many kids will be involved?',
            name: 'numberOfKids',
            step: 3
        }, {
            title: 'Now a little bit of information about marketing...',
            name: 'marketing',
            step: 4
        }, {
            title: 'What will you be selling?',
            name: 'marketingSelection',
            step: 5
        }, {
            title: 'TeamWorks Products',
            name: 'products',
            step: 6
        }, {
            title: 'Add your own products',
            name: 'customProducts',
            step: 7
        }, {
            title: 'Add your Services',
            name: 'customServices',
            step: 8
        }, {
            title: 'A-thon',
            name: 'customAthon',
            step: 9
        }, {
            title: 'What type of Fundraiser are you running?',
            name: 'customOther',
            step: 10
        }, {
            title: 'We need some information about your vendor',
            name: 'vendorInfo',
            step: 11
        }, {
            title: 'Give your campaign a name',
            name: 'campaignName',
            step: 12
        }, {
            title: 'Application Submission',
            name: 'terms',
            step: 13
        }];

        this.currentStep = 1;

        this.productZip = '30120';
    }

    function fundraisingApplication() {
        return {
            restrict: 'E',
            templateUrl: 'app/application/application.html',
            controller: FundraisingApplication,
            controllerAs: 'ctrl',
            scope: {}
        };
    }

    angular.module('app.application', ['app.application.section'])
        .directive('fundraisingApplication', fundraisingApplication);
}());