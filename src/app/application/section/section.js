(function () {
    'use strict';

    function fundraisingApplicationSectionController() {
        var self = this;

        self.productZip = '30290';

        self.back = function (input) {
            if (self.currentStep === 1) {
                return;
            }
            self.currentStep = self.currentStep - 1;

            if (input === 'servicesBack') {
                self.currentStep = 5;
            }
        };
        self.next = function (inputType) {
            self.currentStep = self.currentStep + 1;
            if (angular.isNumber(inputType)) {
                self.currentStep = inputType;
            }

            if (inputType === 'productsForward') {
                self.currentStep = 12;
                console.log(self.currentStep);
            }

            //services
            if (inputType === 'services') {
                self.currentStep = 8;
            }
            if (inputType === 'servicesForward') {
                self.currentStep = 12;
            }

            if (inputType === 'donations') {
                self.currentStep = 12;
            }

            if (inputType === 'athon') {
                self.currentStep = 9;
            }
            if (inputType === 'athonForward') {
                self.currentStep = 12;
            }

            if (inputType === 'customOther') {
                self.currentStep = 10;
            }
            if (inputType === 'customOtherForward') {
                self.currentStep = 12;
            }
        };

        self.fundraisingType = "";
    }

    function fundraisingApplicationSection() {

        function link(scope) {
            scope.ctrl.template = "app/application/section/templates/" + scope.ctrl.ngModel.name + '.html';

        }

        return {
            restrict: 'E',
            templateUrl: 'app/application/section/section.html',
            controller: fundraisingApplicationSectionController,
            controllerAs: 'ctrl',
            link: link,
            scope: {},
            bindToController: {
                ngModel: '=',
                currentStep: '='
            }
        };
    }

    angular.module('app.application.section', [
        'ui.bootstrap'
    ])
        .directive('fundraisingApplicationSection', fundraisingApplicationSection);
}());